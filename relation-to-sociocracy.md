
If you are familiar with Sociocracy, you'll already have figured out
that we're ripping it off wholesale— er, that is, that we're heavily
inspired by and deeply indebted to Sociocracy.

team = circle

Democracy is the rule of the many, with the idea that if at least a bare majority approve of an action (even if that action is to elect a representative), it has some legitimacy.  Sociocracy, rule of the associated, embodies the idea that all people who must carry out a decision have to consent to the course of action.  Everyone gets a say in the direction and conditions of their work.  No one gets to say they are just following orders.

The structure is a bunch of circles, each linked to one or more other circles by a double link: a member of each circle delegated to represent their circle in the other circle.  Therefore, each linked circle has at least two people who are members of both circles.

All decisions made within circles are made by `consent <http://www.sociocracyforall.org/projects/consent-process/>`_, which is basically consensus but sociocracy chooses a different word to put extra emphasis on the fact that everyone in the circle is stating that the collective decision is one they can live with (not one they wholeheartedly endorse).  The decision-making process requires everyone involved be `heard from <http://www.sociocracyforall.org/projects/rounds/>`_, and the process encourages making decisions based on data and scheduling a time to revisit decisions.

Drutopia stays with the more common 'consensus' as it is broadly
understood and used in the same way Sociocracy uses 'consent'.

Drutopia uses the more familiar term *team* rather than *circle* in describing its structure.

