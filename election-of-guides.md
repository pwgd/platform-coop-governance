
Election of Guides
------------------

In the spirit of drawing on outside talent (advocated for in sociocracy), guides do not have to be Drutopia members.

Anyone looking to influence the cooperative through direct representation on the guidance team would form an "interest group" with at least four members.  While demographics does not determine policy, it is a highly visible indicator of inequality of power which we can directly take into account.  Therefore for an interest group of four, at least two people must be women or non-binary and at least two must be people of color (these can of course be the same people, and it still counts).  For larger interest groups the proportion of each must be 40 percent, rounded to the nearest whole human.

Each interest group can state its priorities and reasons for seeking representation on the guidance team, and the full membership votes for their groups they think will provide the best guidance, ranked choice style.  Any group with at least ten percent support among the membership can have a representative on the guidance team (pending consent of the guidance team to that particular person?)

Interest group representation on the guidance team may be single-linked, rather than the more common double link, because there's not expected to be a dedicated communication channel from the guidance team to interest groups, just from interest groups to the guidance team.

Equal access to communication is crucial to equal access to decision-making power.  To that end, any interest group or individual member who has a message they believe to be important enough to go to the full membership will receive review of the message by a dozen other members, and if they concur the message will go out to the full membership.  This allows moderation to prevent an overwhelming number of messages or unwanted messages, but keeps the moderation in the democratic control of the full membership.
