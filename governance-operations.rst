Governance and Operations Structure for PLatform Cooperatives linked to a Free Software Community
===================================================

Overview
--------

(also known as a `LibreSaaS Cooperative <https://libresaas.org/coop>`)

A platform cooperative by design and necessity practices a democracy which puts those who are most affected first.

this extends to building into governance structures attempts to include in decision-making

Drutopia is a democratic project governed internally and in its operations according to sociocratic principles.  




Any team can create another team for advising or carrying out tasks.  Most teams like this are open to anyone willing to take part in doing the work.

Or a team could start independently and seek to affiliate with one or more established teams, and so enter into the formal governance structure

In this a platform cooperative's structure is similar to the structure common to many Free/Libre Open Source Software projects, :doc:`do-ocracy </do-ocracy>` with more accountability.

While working groups can be created and disbanded, keeping Drutopia's structure flexible to expand and adapt as needed, there's a core structure which we'll start out with initially, diagrammed below:

.. image:: images/governance-operations-circles.jpg


Drutopia Software Cooperative Teams
-----------------------------------

.. _guides:
Guides
^^^^^^

The guidance team is made up of the :doc:`elected representatives of the full membership </election-of-guides>`.  It fills the role of what in sociocracy is often called the 'top' circle.

Links
"""""
* :ref:`stewards`
* :ref:`community`
* :ref:`code`


.. _stewards:
Stewards
^^^^^^^^

The stewardship team is the central hub of all work that gets done for the cooperative.  It fills the role of what in sociocracy is often called the 'general' circle.  All, or nearly all, of its members will be in the stewardship team as a consequence of their roles as links to other teams.

Links
"""""
* :ref:`guides`
* :ref:`community`
* :ref:`code`
* :ref:`volunteer-care`
* :ref:`paid-worker-care`
* :ref:`security`
* :ref:`distributions`


.. _community:
Community
^^^^^^^^^

The community team is tasked with maintaining tools used by members of the Drutopia Software Cooperative when helping one another and writing code.

Links
"""""
* :ref:`guides`
* :ref:`stewards`
* :ref:`volunteer-care`

.. _code:
Code
^^^^

The code team, also called the technical working group, is responsible for approving which code gets into Drutopia core and, through linked teams, Drutopia-related distributions.

Links
"""""
* :ref:`guides`
* :ref:`stewards`
* :ref:`security`

.. _outreach:
Outreach & Onboarding
^^^^^^^^^^^^^^^^^^^^^

The outreach and onboarding team is charged primarily with bringing marginalized people into the Drutopia software cooperative, in particular into paid positions and positions of leadership.  This is part of Drutopia's commitment to equality (a requisite precondition to justice and liberty).  Specifically, the outreach and onboarding team is responsible for making and keeping Drutopia inclusive regarding gender, gender identity, sexual orientation, ethnicity, ability, age, religion, geography, and class.

Making and keeping Drutopia live its values of justice and equality goes much deeper than outreach and onboarding.  Outreach and onboarding, as responsible for being most in touch with many disadvantaged people, plays a role in helping all other teams improve their actions on this front.  Alternatively, if this is overloading one team, we should create an 'internal justice' or such team for this overriding goal.

  Rather than tech diversity and inclusion, we should instead seek tech equity and justice: explicitly political, less politically empty and mutable language. This is, of course, a far broader and more challenging goal. The explicit political, anti-oppressive nature of tech equity and justice will undoubtedly draw more fire from those committed to maintaining the existing hierarchies and inequities in tech. Additionally, tech equity and justice would need to address a far wider range of inequities and injustices connected to tech, from the racist gentrification of San Francisco and the East Bay, to exploitative labor practices required for the abundance of cheap electronics and devices we consume as techies, to the environmental and human destruction wreaked in order to obtain the basic materials for creating those devices.

  Yes, tech equity and justice is a much more challenging road for us to walk, but that is because it gets to the roots of the systems that have made tech diversity and inclusion efforts necessary in the first place. And when you get at the roots of a problem, you’re far more likely to get rid of the problem for good.

  -- Jack Aponte, "`The centre cannot hold: on diversity and inclusion in tech <http://jackalop.es/2017/08/07/centre-cannot-hold-diversity-inclusion-tech/>`_"

Secondarily, the members of the outreach team work to ensure that members of all teams are actively involved in organizations that fit the profile of organizations, of grassroots groups, that Drutopia exists to support.

Links
"""""
* :ref:`guides`
* :ref:`stewards`
* :ref:`paid-worker-care`
* :ref:`volunteer-care`

.. _volunteer-care:
Volunteer care
^^^^^^^^^^^^^^

Links
"""""
* :ref:`stewards`
* :ref:`paid-worker-care`
* :ref:`outreach`

.. _paid-worker-care:
Paid worker care
^^^^^^^^^^^^^^^^

Links
"""""
* :ref:`stewards`
* :ref:`volunteer-care`
* :ref:`outreach`

.. _security:
Security & Privacy
^^^^^^^^^^^^^^^^^^

The security and privacy team is responsible for oversight of security, the protection of personal information, privacy, and freedom from surveillance— both in the Drutopia codebase and Drutopia Software Cooperative's own application of Drutopia code and other software.

In addition, the security and privacy team works with the security teams of approved platforms to review security and privacy practices.

Links
"""""
* :ref:`stewards`
* :ref:`code`
* :ref:`community`

.. _distributions:
Distributions
^^^^^^^^^^^^^

The distributions team 

.. `saas`
SaaS

.. `_platform-coops`
Platform cooperatives
^^^^^^^^^^^^^^^^^^^^^

The platform cooperatives team is tasked with approving Drutopia-providing platform cooperatives and representing their interests to the Drutopia Software Cooperative.

#Question  Even if we had a separate circle approve platform coops, the platform coop circle would have to consent to a representative of an accepted platform coop joining the circle.  Drutopia wants to encourage cooperation not competition, but — matters of interpretation aside — representatives of platform coops already represented in this circle may have   .  The same thing might happen with seating representatives of the elected interest groups.


Drutopia Platform Cooperative Teams
-----------------------------------

The first thing to point out about the platform cooperative is that the single Drutopia Software Cooperative is set up to work with any number of platform cooperatives, or indeed other hosting providers which may not be organized as platform cooperatives but still meet the 

